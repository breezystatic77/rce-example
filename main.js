$(document).ready(() => {
	let scrollingEvents = {
		10: {
			down: () => {

			},
			up: () => {

			}
		},
		300: {
			down: () => {
				$('.pitcher').css('opacity', '0');

			},
			up: () => {
				$('.pitcher').css('opacity', '1');

			}
		},
		600: {
			down: () => {
				showPic(1, 0);
			},
			up: () => {
				showPic(1, -1);
			}
		},
		900: { 
			down: () => {
				showPic(2, 0);
			},
			up: () => {
				showPic(2, -1);
			}
		},
		1200: {
			down: () => {
				showPic(1, 1);
			},
			up: () => {
				showPic(1, 0);
			}
		},
		1500: { 
			down: () => {
				showPic(2, 1);
			},
			up: () => {
				showPic(2, 0);
			}
		},
		1800: { 
			down: () => {
				showPic(1, 2);
			},
			up: () => {
				showPic(1, 1);
			}
		},
		2100: { 
			down: () => {
				showPic(2, 2);
			},
			up: () => {
				showPic(2, 1);
			}
		},
		2400: { 
			down: () => {
				showPic(1, 3);
			},
			up: () => {
				showPic(1, 2);
			}
		},
		2700: { 
			down: () => {
				showPic(2, 3);
			},
			up: () => {
				showPic(2, 2);
			}
		}
	};

	function showPic(whichPiccy, index) {
		let piccy;
		if(whichPiccy == 1) piccy = $('.piccy#one');
		else if (whichPiccy == 2) piccy = $('.piccy#two');
		piccy.children().css('opacity', '0');
		if(index != -1) {
			piccy.children().eq(index).css('opacity', '1');
		}
	}


	// ugly bit

	function doScrollEvent(num, isDownward) {
		if(scrollingEvents[num] != undefined) {
			if(isDownward) scrollingEvents[num].down();
			else scrollingEvents[num].up();
		}
	}

	let oldScroll = 0;
	$('.scrollyboy').scroll(() => {
		let newScroll = $('.scrollyboy').scrollTop();
		if(newScroll > oldScroll) { // down
			for(let i = oldScroll; newScroll > i; i++) {
				doScrollEvent(i, true);
			}
		} else if (oldScroll > newScroll) { // up
			for(let i = newScroll; !(i > oldScroll); i++) {
				doScrollEvent(i, false);
			}
		}
		console.log(newScroll);
		oldScroll = newScroll;
	});
});