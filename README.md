This is an example profile that can be compiled using [rph-coding-env](https://bitbucket.org/breezystatic77/rph-coding-env/). All information on how to use it can be found under that link.

For more information on Pug, [this is a useful tutorial](https://flaviocopes.com/pug/). The official website is [here](https://pugjs.org/).

For more information on SCSS, their tutorial is [here](https://sass-lang.com/guide).